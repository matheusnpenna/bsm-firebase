import React from 'react';
import {
  Image,
  FormControl,
  Form,
  InputGroup,
  Alert,
  ProgressBar
} from 'react-bootstrap';
import firebase from 'firebase';
import axios from 'axios';
import { Storage } from '../../services';
import { PhonePreview, Button, ConfirmationModal } from '../../components';
import { assets } from '../../assets'

import './styles.css';
const FUNCTION_URL = 'https://us-central1-brasil-sem-medo-fcf4b.cloudfunctions.net/sendNotification'

const CLEARED_STATE = {
  loading: false,
  title: '',
  body: '',
  imgFile: null,
  name: '',
  segmentation: '',
  previewExpand: false,
  warning: '', 
  successWarning: '',
  token: null,
  code: '',
  id: null,
  url: null,
  goal: '',
  isUpload: false,
  uploadProgress: 0,
  showConfirmation: false
};
class HomeScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      ...CLEARED_STATE
    }
  }
 
  uploadFile = (file, callback = null) => {
        const uploadTask = Storage.ref().child(`images/notifications/${file.name}`).put(file);

        uploadTask.on('state_changed', (snapshot) => {
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              this.setState({
                isUpload: 0,
                uploadProgress: (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              });
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              this.setState({ isUpload: true, uploadProgress: (snapshot.bytesTransferred / snapshot.totalBytes) * 100 })
              break;
            default:
              break;
          }
        }, (error) => {
          // Handle unsuccessful uploads
        }, () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
            if (callback) {
              callback(downloadURL)
            }
          });
        });
  }

  onConfirmation = () => {
    const { imgFile, loading } = this.state
    if (!loading) {
      this.setState({ loading: true })
      if (imgFile) {
        this.uploadFile(imgFile, this.sendNotification)
      } else {
        this.sendNotification()
      }
    }
  }

  showConfirmationModal = () => {
    const {
      title,
      body,
      segmentation,
      token,
      id,
      url,
      goal
    } = this.state

    if (!goal || goal === '') { this.setState({ warning: 'Escolha a finalidade da notificação!'}); return; }

    if (goal === 'article' && !id) { this.setState({ warning: 'Por favor, insira o id do artigo!'}); return; }
    
    if (goal === 'link' && !url) { this.setState({ warning: 'Por favor, insira a URL a ser aberta no app. (Use sempre https)!'}); return; }

    if (!title || title === 'Titulo da Notificação') { this.setState({ warning: 'Digite o título da notificação!'}); return; }

    if (!body || body === 'Texto da notificação') { this.setState({ warning: 'Digite o texto da notificação!'}); return; }
    
    if (body.length > 1000) {
      this.setState({ warning: 'O texto da sua notificação é muito grande! Por favor, use menos que 1000 caracteres!'});
      return;
    }

    if (segmentation === 'none' || segmentation === '' || !segmentation) { this.setState({ warning: 'Especifique o grupo que receberá a notificação!'}); return; }

    if (segmentation === 'one' && !token) { this.setState({ warning: 'Insira o token do dispositivo a receber a notificação!'}); return; }

    this.setState({ showConfirmation: true })
  }

  closeConfirmation = () => this.setState({ showConfirmation: false })

  successHandle = response => {
    document.getElementById('custom-file').value = ''
    this.setState({ ...CLEARED_STATE, successWarning: 'Notificação Enviada!', showConfirmation: false })
  }

  errorHandle = error => this.setState({ warning: error.message, loading: false })

  sendNotification = (imgUrl = null) => {
    const {
      title,
      body,
      segmentation,
      token,
      id,
      url,
      goal
    } = this.state

    let notification: Notification = { title, body }
    
    let data = {}

    if (imgUrl && imgUrl !== '') { notification = { ...notification, imageUrl: imgUrl } }
    
    if (goal === 'article' || goal === 'link') { data = { code: goal } }
    
    if (id) { data = { ...data, id: id } }
    
    if (url) { data = { ...data, url: url } }

    if (segmentation === 'general' || segmentation === 'premium') {
      axios({
        method: 'post',
        url: FUNCTION_URL,
        data: { topic: segmentation, ...notification, ...data }
      })
      .then(this.successHandle)
      .catch(this.errorHandle);
    } else {
      axios({
        method: 'post',
        url: FUNCTION_URL,
        data: { token, ...notification, ...data }
      })
        .then(this.successHandle)
        .catch(this.errorHandle);
    }
  }

  expand = () => this.setState({ previewExpand: !this.state.previewExpand })

  onChangeTitle = (event) => this.setState({ title: event.target.value })

  onChangeBody = (event) => {
    if (event.target.value.length >= 1000) {
      this.setState({ warning: 'O texto da sua notificação é muito grande! Por favor, use menos que 1000 caracteres!'});
      return;
    }

    this.setState({ body: event.target.value, warning: '' })
  }

  onChangeImgFile = (event) => {
    if (event.target.files[0] && event.target.files[0].size <= 300000) {
      this.setState({ imgFile: event.target.files[0], warning: '' })
    } else {
      this.setState({ warning: 'O arquivo de imagem tem o limite de 300 kb', loading: false })
    }
  }
  
  onChangeName = (event) => this.setState({ title: event.target.value, successWarning: '' })

  onChangeSegmentation = (event) => this.setState({ segmentation: event.target.value, successWarning: '' })

  onChangeToken = (event) => this.setState({ token: event.target.value, successWarning: '' })

  onChangeGoal = (event) => this.setState({ goal: event.target.value, id: null, url: null, successWarning: '' })

  onChangeId = (event) => this.setState({ id: event.target.value, successWarning: '' })

  onChangeUrl = (event) => this.setState({ url: event.target.value, successWarning: '' })

  renderNotificationInPreview = (visualization = null) => {
    const { title, body, imgFile, previewExpand } = this.state

    if (previewExpand) {
      return (
        <div className="notification-preview-container">
          <h1 className="title">{title.length > 0 ? title : 'Título da notificação'}</h1>
          <h3 className="text-expanded" style={{ maxHeight: 120 }}>{body.length > 0 ? body : 'Texto da notificação'}</h3>
          {!!imgFile &&
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
              <Image
                src={imgFile ? URL.createObjectURL(imgFile) : assets.images.imagePlaceholder}
                className="img-placeholder-expanded"
                style={{ width: visualization === 'short' ? '50%' : '100%', height: visualization === 'short' ? 80 : '100%', alignSelf: 'center'  }}
              />
            </div>
          }
        </div>
      )
    }

    return (
      <div className="notification-preview-container row">
        <div className="n-text-ct column">
          <h1 className="title">{title.length > 0 ? title : 'Título da notificação'}</h1>
          <h3 className="text" style={{ maxHeight: 50, textOverflow: 'ellipsis', overflow: 'hidden' }} >{body.length > 0 ? body : 'Texto da notificação'}</h3>
        </div>
        <div className="n-img-ct" style={{ justifyContent: 'center', alignItems:'center', paddingHorizontal: 10 }}>
            <Image src={imgFile ? URL.createObjectURL(imgFile) : assets.images.imagePlaceholder} className="img-placeholder" />
        </div>
      </div>
    )
  }

  render() {
    const {
      loading,
      previewExpand,
      segmentation,
      goal,
      warning,
      successWarning,
      isUpload,
      uploadProgress,
      showConfirmation,
      imgFile,
      title,
      body,
      token,
    } = this.state;

    return (
      <div className="container-home">
          <Image src={assets.images.logo} className="img-logo" />
          {!!warning && 
            <InputGroup className="custom-input-group">
              <Alert variant={'danger'}>
                {warning}
              </Alert>
            </InputGroup>
          }
          {!!successWarning && 
            <InputGroup className="custom-input-group">
              <Alert variant={'success'}>
                {successWarning}
              </Alert>
            </InputGroup>
          }
          <div className="row">
              <div className="column ">
                  <Form.Group controlId="exampleForm.SelectCustom">
                    <Form.Label className="select-label">Finalidade da Notificação</Form.Label>
                    <Form.Control value={goal} onChange={this.onChangeGoal} as="select" custom>
                      <option value="none">Selecione...</option>
                      <option value="article">Novo artigo</option>
                      <option value="link">Envio de link para abrir no app</option>
                      <option value="other">Outro - Informativo</option>
                    </Form.Control>
                  </Form.Group>
                  {(goal === 'article') &&   <FormControl className="common-input" type="number" onChange={this.onChangeId} placeholder="ID do artigo" />}
                  {(goal === 'link') &&   <FormControl className="common-input" onChange={this.onChangeUrl} placeholder="URL a ser aberta no app. Obs: Use sempre com https" />}
                  <FormControl value={title} className="common-input" onChange={this.onChangeTitle} placeholder="Título da notificação" />
                  <FormControl value={body} className="common-input" onChange={this.onChangeBody} as="textarea" rows="3"  placeholder="Texto da notificação" />
                  <Form.File
                    accept="image/*"
                    onChange={this.onChangeImgFile}
                    className="img-input"
                    id="custom-file"
                    label={imgFile ? imgFile.name : "Imagem da notificação (opcional). Obs: tamanho máximo 300kb"}
                    data-browse="Buscar"
                    custom
                  />
                  {/* <FormControl className="common-input" onChange={this.onChangeName} placeholder="Nome da notificação (opcional)" /> */}
                  <Form.Group controlId="exampleForm.SelectCustom">
                    <Form.Label className="select-label">Grupo que receberá a notificação:</Form.Label>
                    <Form.Control value={segmentation} onChange={this.onChangeSegmentation} as="select" custom>
                      <option value="none">Selecione...</option>
                      <option value="general">Todos</option>
                      <option value="premium">Somente usuários pagantes</option>
                      <option value="one">Para um dispositivo específico</option>
                    </Form.Control>
                  </Form.Group>
                {(segmentation === 'one') &&
                  <FormControl value={token} className="common-input" onChange={this.onChangeToken} as="textarea" rows="2"  placeholder="Token do dispositivo do usuário" />
                }
                <Button onClick={this.showConfirmationModal} loading={loading} label={'Enviar'} />
            </div>
            <div className="column centering-w-padding">
                <PhonePreview
                    renderComponent={this.renderNotificationInPreview}
                    buttonOnClick={this.expand}
                    buttonLabel={previewExpand ? 'Visualização normal' : 'Visualização expandida'} />
            </div>
            <ConfirmationModal
                isUpload={isUpload}
                renderProgressBar={<ProgressBar now={uploadProgress} />}
                data={this.state}
                show={showConfirmation}
                onHide={this.closeConfirmation}
                renderConfirmationButton={<Button onClick={this.onConfirmation} loading={loading} label={'Enviar'} />}
                PhonePreview={
                  <PhonePreview
                    visualization={'short'}
                    renderComponent={() => this.renderNotificationInPreview('short')}
                    buttonOnClick={this.expand}
                    buttonLabel={previewExpand ? 'Visualização normal' : 'Visualização expandida'} />}
            />
        </div>
      </div> 
    );
  }
}

export default HomeScreen;
