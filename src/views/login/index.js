import React from 'react';
import {
  InputGroup,
  FormControl,
  Image,
  Button,
  Container,
  Spinner,
  Alert
} from 'react-bootstrap';
import { Auth } from '../../services';
import { assets } from '../../assets';
import './styles.css';

class LoginScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      error: { visible: false, message: '' },
      loading: false
    }
  }

  onEmailChange = (event) => {
    this.setState({ email: event.target.value });
  }

  onPasswordChange = (event) => {
    this.setState({ password: event.target.value });
  }

  attemptLogin = () => {
    const { email, password } = this.state;
    this.setState({ loading: true });
    Auth
    .signInWithEmailAndPassword(email, password)
    .then(() => {
      this.setState({ loading: false }, () => {
        this.props.history.push('/');
        this.props.updateLoginStack(true);
      })
    })
    .catch((error) => {
      this.setState({ 
          error: {
            visible: true,
            message:`Error ${error.code} - ${error.message}` },
          loading: false
      });
    });

  }
    render() {
      const { email, password, error, loading } = this.state;
    
      return (
        <Container className="container" >
            <InputGroup className="custom-input-group">
                  <Image src={assets.images.logo} className="img-logo" />
                  <h1 className="text-center text-dark label">Faça login</h1>
            </InputGroup>
            {error.visible &&
              <InputGroup className="custom-input-group">
                <Alert variant={'danger'}>
                  {error.message}
                </Alert>
              </InputGroup>
            }
            <InputGroup className="custom-input-group input-width">
                <FormControl className="custom-input" placeholder='E-mail. ex: username@provider.com' type="text" value={email} onChange={this.onEmailChange} />
            </InputGroup>
            <InputGroup className="custom-input-group input-width">
                <FormControl className="custom-input" placeholder='password' type="password" value={password} onChange={this.onPasswordChange} />
            </InputGroup>
            <InputGroup className="custom-input-group input-width">
              <Button variant="primary" size="lg" block onClick={this.attemptLogin}>
              {
                  loading ? 
                  <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                  :
                  'Login'
                }
              </Button>
            </InputGroup>
        </Container> 
      );
    }
}

export default LoginScreen;
