import React from "react";
import { Link } from "react-router-dom";
import { Nav, Button } from 'react-bootstrap';
import { SideBarButton } from '../../components';
import { Auth } from '../../services';
import './styles.css';

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vizualization: 'sidebar-closed' 
    };
  }

  doLogout = () => {
    Auth.signOut().then(() => {
      this.props.updateLoginStack();
    }).catch(function(error) {
      console.log(`Deu ruim - ${error}`);
    });
  }

  render() {
      const {
          routes,
          visible,
          onClosed
      } = this.props;
      const vizualization = visible ? 'sidebar-opened' : 'sidebar-closed';

    return (
        <div className={`sidebar ${vizualization}`}>
            <div className="float-right">
                <SideBarButton action={onClosed} />
            </div>
            <Nav defaultActiveKey="/" className="flex-column nav-body">
              <Nav.Link><Link to="/">{routes.home.name}</Link></Nav.Link>
              <Nav.Link><Button onClick={this.doLogout} variant="Link">Logout</Button></Nav.Link>
            </Nav>
        </div>
    );
  }
}

export default Sidebar;