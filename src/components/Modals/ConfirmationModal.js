import React, { Component } from 'react';
import { Modal, Button, Col, Row } from 'react-bootstrap';

class ConfirmationModal extends Component {
  constructor(props) {
      super(props)
      this.state = {

      }
  }

  renderGoal = (data) => {
      if (data.goal === 'general') return 'Novo Ártigo';

      if (data.goal === 'premium') return 'Abertura de Link no App';

      if (data.goal === 'not-premium') return 'Abertura de Link no App';

      return 'Notifcação Simples (Informação)';
  }

  renderSegmentation = (data) => {
    if (data.segmentation === 'general') return 'Todos os usuários';

    if (data.segmentation === 'premium') return 'Somente usuários pagantes';

    return 'Dispositivo Específico para Teste';
  }

  render() {
    const { data, PhonePreview, renderConfirmationButton } = this.props
    return <Modal
                {...this.props}
                size="xl"
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title style={{ color: '#000'}} id="contained-modal-title-vcenter">Confirme as informações enviadas na notificação</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col>
                            <h5 style={{ color: '#000'}}>GRUPO QUE RECEBERÁ A NOTIFIÇÃO:</h5><h6 style={{ color: '#5E5E64'}}>{this.renderSegmentation(data)}</h6>
                            <h5 style={{ color: '#000'}}>TÍTULO:</h5><h6 style={{ color: '#5E5E64'}}>{data.title}</h6>
                            <h5 style={{ color: '#000'}}>SUBTÍTULO:</h5><h6 style={{ color: '#5E5E64', textOverflow: 'ellipsis', overflow: 'hidden', wordBreak: 'break-all' }}>{data.body}</h6>
                            <h5 style={{ color: '#000'}}>FINALIDADE:</h5><h6 style={{ color: '#5E5E64'}}>{this.renderGoal(data)}</h6>
                            {data.id ?  <div><h5 style={{ color: '#000'}}>ID DO ARTIGO:</h5> <h6 style={{ color: '#5E5E64'}}>{data.id}</h6></div> : null}
                            {data.url ?  <div><h5 style={{ color: '#000'}}>URL A SER ABERTA:</h5><h6 style={{ color: '#5E5E64'}}>{data.url}</h6></div> : null}
                        </Col>
                        <Col>
                            {PhonePreview}
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <div style={{ width: 100 }}>{renderConfirmationButton}</div>
                    <Button onClick={this.props.onHide} style={{ height: 40 }} >Cancelar</Button>
                </Modal.Footer>
            </Modal>
  }
}
export default ConfirmationModal;
