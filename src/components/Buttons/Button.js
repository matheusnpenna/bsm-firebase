import React from 'react';
import {
  Button,
  Spinner
} from 'react-bootstrap';

class HomeScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    }
  }

  render() {
    const { onClick, loading, label } = this.props;

    return (
        <Button {...this.props} variant="primary" size="lg" block onClick={onClick}>
            {
                loading ? 
                <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />
                :
                label
            }
        </Button>
    );
  }
}

export default HomeScreen;
