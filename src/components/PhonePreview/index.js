import React from 'react';
import { Button } from '../'
import './styles.css'
class PhonePreview extends React.Component {

  render() {
    const { visualization, renderComponent, buttonOnClick, buttonLabel } = this.props
    return (
        <div id="inner_wrap">  
            <div
              id="wrapper"
              style={{
                width: 360,
                height: visualization === 'short' ? 280 : 640
              }}>
                <div className={`${visualization === 'short' ? 'phone view_3' : 'phone view_2'}`} id="phone_1"
                  style={{
                    width: 360,
                    height: 640
                  }}>
                    {renderComponent()}
                    <div className="btn-act"><Button style={{ fontSize: 10 }} onClick={buttonOnClick} loading={false} label={buttonLabel} /></div>
                </div>
            </div>
        </div>
    )
  }
}

export default PhonePreview;