const assets = {
    images: {
        logo: require('./logo.png'),
        imagePlaceholder: require('./image_placeholder.png')
    },
    icons: {
        hamburguer: require('./menu.png')
    }
}

export {
    assets
}