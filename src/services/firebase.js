import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAINasD7t3qaXJ8sTjonKw8iSApQ6wNBoQ",
    authDomain: "brasil-sem-medo-fcf4b.firebaseapp.com",
    databaseURL: "https://brasil-sem-medo-fcf4b.firebaseio.com",
    projectId: "brasil-sem-medo-fcf4b",
    storageBucket: "brasil-sem-medo-fcf4b.appspot.com",
    messagingSenderId: "973560178078",
    appId: "1:973560178078:web:7a96411c6b4c85bb57228f",
    measurementId: "G-QY9KG4CV7Y"
};

firebase.initializeApp(firebaseConfig);

export const RealtimeDatabase = firebase.database();
export const Storage = firebase.storage();
export const FireStore = firebase.firestore();
export const Auth = firebase.auth();