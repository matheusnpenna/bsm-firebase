import {
    HomeScreen,
    LoginScreen
} from '../views';

export const routes = {
    login: { name: 'Login', path:'/login', component: LoginScreen },
    home: { name: 'Home', path:'/', component: HomeScreen },
};