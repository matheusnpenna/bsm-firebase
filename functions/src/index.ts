import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
const cors = require('cors')({ origin: true })

// var serviceAccount = require("/Users/samuel/Downloads/brasil-sem-medo-fcf4b-firebase-adminsdk-iq23s-bbec6d2219.json");

admin.initializeApp({
    credential: admin.credential.applicationDefault()
});

interface NotificationInterface {
    title: string,
    body: string,
    image?: string,
}

export const sendNotification = functions.https.onRequest((request, response) => {
    response.set('Access-Control-Allow-Origin', '*')
    cors(request, response, () => {
        try {
            const {
                title,
                body,
                imageUrl,
                topic,
                code,
                id,
                url,
                token
            } : {
                title: string,
                body: string,
                imageUrl?: string,
                topic?: string,
                code?: string,
                id?: string,
                url?: string,
                token?: string
            } = request.body
        
            if (!title) throw new Error('no_title_provided')
            if (!body) throw new Error('no_body_provided')
            
            let notification: NotificationInterface = { title, body }
            let data = {}
            
            if (imageUrl) { notification = { ...notification, image: imageUrl } }
            
            if (code) { data = { code: code } }
    
            if (code === 'article' && !id) throw new Error('no_id_provided_for_code_article')
            
            if (code === 'link' && !url) throw new Error('no_url_provided_for_code_link')
            
            if (id) {
                data = { ...data, id: id }
            }
            
            if (url) {
                data = { ...data, url: url }
            }
    
            if (token) {
                return admin
                .messaging()
                .send({
                    token,
                    notification,
                    data
                })
                .then(() => response.status(200).json({ success: true }))
                .catch((e) => response.status(500).json({ error: e.message }))
            }
            
            return admin
                .messaging()
                .send({
                    topic: topic ? topic : 'general',
                    notification,
                    data
                })
                .then(() => response.status(200).json({ success: true }))
                .catch((e) => response.status(500).json({ error: e.message }))
        } catch (error) {
            return response.status(500).json({ error: error.message })
        }
    })
})
